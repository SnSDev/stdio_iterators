use std_io_iterators::prelude::*;

fn main() -> Result<(), std::io::Error> {
    // Pipe in, lazy transform, pipe out
    match PipeInIterator::try_new()
        .expect("1669235203 - Unable to lock STDIN")
        .map(INTERNAL_FUNCTION)
        .pipe_out()
    {
        Err(e) if e.is_broken_pipe() => {
            // Log Broken Pipe
            return Ok(());
        }
        Err(e) => return e.dump_data().into(),
        _ => (),
    }

    // Pipe out array, re-direct additional data
    if let Err(mut e) = (["test1", "test2", "test3"].iter()).pipe_out() {
        e.by_ref()
            .for_each(|_recovered_datum| todo!("Handle recovered data"));
        return e.result.into();
    }

    Ok(())
}

const INTERNAL_FUNCTION: fn(std::rc::Rc<String>) -> String =
    |line| format!("Prepend-{}", line);
