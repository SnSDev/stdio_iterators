use std_io_iterators::prelude::*;

fn main() -> Result<(), std::io::Error> {
    // Collect everything piped in, check if expected values
    let pipe_in = PipeInIterator::try_new()
        .expect("1669234183 - Unable to establish pipe")
        .map(|v| v.to_string())
        .collect::<Vec<String>>();

    assert_eq!(
        pipe_in,
        vec![
            "Test1".to_string(),
            "Test2".to_string(),
            "Test3".to_string()
        ]
    );

    println!("1669234324 - Test Passed");

    Ok(())
}
