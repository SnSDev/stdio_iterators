use std_io_iterators::prelude::*;

fn main() -> Result<(), std::io::Error> {
    let mut pipe_in = PipeInIterator::try_new()
        .expect("1669234431 - Unable to establish pipe");

    // Accept 2 lines then exit gracefully
    println!("{}", pipe_in.next().unwrap());
    println!("{}", pipe_in.next().unwrap());

    Ok(())
}
