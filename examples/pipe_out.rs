use std_io_iterators::prelude::*;

fn main() -> Result<(), std::io::Error> {
    // Pipe out 3 values (expected for `pipe_in` example)
    ["Test1", "Test2", "Test3"]
        .iter()
        .pipe_out()
        .map_err(|e| e.result.into())
}
