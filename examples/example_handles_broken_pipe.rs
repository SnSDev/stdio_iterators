use std_io_iterators::prelude::*;

fn main() -> Result<(), std::io::Error> {
    // Pipe out array, send anything that does not get piped out to `STDERR`
    eprintln!("Launched");

    if let Err(mut e) = (["Test1", "Test2", "Test3"].iter()).pipe_out() {
        e.by_ref().for_each(|recovered_datum| {
            eprintln!("{recovered_datum} - Recovered")
        });

        eprintln!("Recovery Done");
        return Ok(());
    }

    eprintln!("Finished");
    Ok(())
}
