//! An iterator for `STDIN` and a wrapper for `STDOUT`. Allows easy piping, and
//! graceful closing of application if pipe breaks
//!
//! *Project by [SnS Development](https://gitlab.com/SnSDev)*
//!
//! # Problem
//!
//! Using `STDIN` and `STDOUT` (piping in or piping out) is more complicated
//! than is necessary.  The developer should be able to obtain an iterator for
//! the data being piped in through `STDIN`, and should be able to easily pipe
//! out through `STDOUT` any iterator of elements implementing the [std::fmt::Debug] trait.
//!
//! Also, if the `STDOUT` pipe is "broken" (i.e. the data being piped into
//! exits), the iterator being piped out through `STDOUT` should be recoverable
//! so the data can be re-directed. (See Warning in
//! [`prelude::PipeOut::pipe_out`])
//!
//! # Solution
//!
//! - A struct [prelude::PipeInIterator] - An [Iterator] that abstracts the
//!   process of piping in through `STDIN`
//! - A trait [prelude::PipeOut] - Implemented on any [Iterator] over elements
//!   that implement [std::fmt::Debug] (like [String]) that abstracts the
//!   process of piping out through `STDOUT`
//!
//! # Example
//!
//! ```rust,no_run
#![doc = include_str!("examples/demo.rs")]
//! ```
//!
//! # How To: Update Documentation
//!
//! The documentation in this project is contained in the documentation comments
//! throughout the code.  Such comments are then compiled into the final
//! documentation for the project via `rustdoc` when the code is published.
//!
//! It is good practice to include a `README.md` file in root folder of the
//! source code for displaying in the respective version management system.  In
//! compliance with DRY principle:
//!
//! <blockquote>
//! Every piece of knowledge must have a single, unambiguous, authoritative
//! representation within a system
//!
//! - Src: [Don't repeat yourself - Wikipedia](https://en.wikipedia.org/wiki/Don't_repeat_yourself)
//! </blockquote>
//!
//! and in-order to keep the file from getting out-of-sync with the
//! documentation, I simply duplicate the root page of the documentation via the
//! enclosed script.
//!
//! ## `update_documentation.sh` *Run in the root of project*
//!
//! ```bash
#![doc = include_str!("update_documentation.sh")]
//! ```
#![warn(missing_docs)]
#![warn(clippy::pedantic)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_to_string)]

pub mod pipe_in_iterator;
pub mod pipe_out;
pub mod pipe_out_recovered_iterator;

pub mod prelude {
    //! Re-exports all components as one module that can be glob imported.

    pub use crate::{
        pipe_in_iterator::PipeInIterator,
        pipe_out::{PipeOut, WriteLineResult},
        pipe_out_recovered_iterator::PipeOutRecoveredIterator,
    };
}
