use pretty_assertions::assert_eq;
use std::{io::Write, process::Stdio};

#[test]
fn test1653483581_demo_example() {
    // GIVEN
    let mut std_io_iterators_demo = escargot::CargoBuild::new()
        .example("demo")
        .run()
        .expect("1653483983 - Failed to link to cargo binary")
        .command()
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .expect("1653483903 - Failed to spawn child process");

    let mut stdin = std_io_iterators_demo
        .stdin
        .take()
        .expect("1653485873 - Failed to open stdin");

    // WHEN
    std::thread::spawn(move || {
        stdin
            .write_all("Hello, world!".as_bytes())
            .expect("1653484109 - Failed to write to stdin");
    });

    let output = std_io_iterators_demo
        .wait_with_output()
        .expect("1653484120 - Failed to read stdout");

    // THEN
    assert_eq!(
        String::from_utf8_lossy(&output.stdout),
        "Prepend-Hello, world!\ntest1\ntest2\ntest3\n"
    );
}

#[test]
fn test1669403991_pipe_in_and_pipe_out_examples() {
    // GIVEN
    let pipe_out = escargot::CargoBuild::new()
        .example("pipe_out")
        .run()
        .expect("1669404035 - Failed to link to pipe_out")
        .command()
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .expect("1669404297 - Failed to spawn pipe_out");

    let std_out = pipe_out.stdout.unwrap();

    let pipe_in = escargot::CargoBuild::new()
        .example("pipe_in")
        .run()
        .expect("1669404314 - Failed to link to pipe_in")
        .command()
        .stdin(std_out)
        .stdout(Stdio::piped())
        .spawn()
        .expect("1669404321 - Failed to spawn pipe_in");

    // WHEN
    let output = pipe_in
        .wait_with_output()
        .expect("1669404902 - Failed to read stdout");

    // THEN
    assert_eq!(
        String::from_utf8_lossy(&output.stdout),
        "1669234324 - Test Passed\n"
    );
}

// #[test]
// fn test1669406351() {
//     // GIVEN
//     let pipe_out = escargot::CargoBuild::new()
//         .example("example_handles_broken_pipe")
//         .run()
//         .expect("1669406411 - Failed t  o link to example_handles_broken_pipe")
//         .command()
//         .stdin(Stdio::piped())
//         .stdout(Stdio::piped())
//         .stderr(Stdio::piped())
//         .spawn()
//         .expect("1669406418 - Failed to spawn example_handles_broken_pipe");

//     let std_out = pipe_out.stdout.unwrap();
//     let mut std_err = pipe_out.stderr.unwrap();

//     let break_pipe = escargot::CargoBuild::new()
//         .example("break_pipe")
//         .run()
//         .expect("1669406457 - Failed to link to break_pipe")
//         .command()
//         .stdin(std_out)
//         .stdout(Stdio::piped())
//         .stderr(Stdio::piped())
//         .output()
//         .expect("1669406461 - Failed to spawn break_pipe");

//     let bp_handle = std::thread::spawn(move || {
//         println!(
//             "Out: {}\nErr: {}",
//             String::from_utf8_lossy(&break_pipe.stdout),
//             String::from_utf8_lossy(&break_pipe.stderr)
//         );
//     });
//     let mut buf = String::new();
//     std_err.read_to_string(&mut buf).unwrap();
//     bp_handle.join().unwrap();
//     assert_eq!(buf, String::new());
//     // THEN
//     assert_eq!(buf, "Test2\nTest3");
// }
